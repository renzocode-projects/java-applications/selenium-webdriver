# SELENIUM WEBDRIVER
## CHAPTER 3
### WEBDRIVER FUNDAMENTALS
#### Basic WebDriver Usage

This section covers three fundamental aspects related to WebDriver objects

##### WebDriver Creation

The first step is to create WebDriver instances. Thus, we need to create a Chrome
Driver object when using Chrome, EdgeDriver for Edge, FirefoxDriver for Firefox,
and so on

```
WebDriver driver = new ChromeDriver();
```

##### WebDriver builder
The Selenium WebDriver API provides a built-in method following the builder pat‐
tern to create WebDriver instances. This feature is accessible through the static
method builder() of the RemoteWebDriver class and provides a fluent API for creat‐
ing WebDriver objects.
![alt text](./images/photo_2023-10-11_10-38-39.jpg)


##### WebDriver Methods

The WebDriver interface provides a group of methods that are the basis of the Sele‐
nium WebDriver API.

![alt text](./images/photo_2023-10-11_10-48-33.jpg)

![alt text](./images/photo_2023-10-11_13-06-19.jpg)

```
@Test
void testBasicMethods() {
	String sutUrl =  "https://bonigarcia.dev/selenium-webdriver-java/";
	this.driver.get(sutUrl);
	assertThat(this.driver.getTitle())
			.isEqualTo("Hands-On Selenium WebDriver with Java");
	assertThat(this.driver.getCurrentUrl()).isEqualTo(sutUrl);
	assertThat(this.driver.getPageSource()).containsIgnoringCase("</html>");
}
```
Execute this command

```
mvn test -Dtest="WebDriverBuilderJupiterTest#testBasicMethods"
```

##### Session Identifier

Each time we instantiate a WebDriver object, the underlying driver (e.g., chrome‐
driver, geckodriver, etc.) creates a unique identifier called sessionId to track the
browser session. We can use this value in our test to univocally identify a browser ses‐
sion. For that, we need to invoke the method getSessionId() in our driver object.

```
@Test
void testSessionId() {
	this.driver.get("https://bonigarcia.dev/selenium-webdriver-java/");
	SessionId sessionId = ((RemoteWebDriver) driver).getSessionId();
	assertThat(sessionId).isNotNull();
	log.debug("The sessionId is {}", sessionId.toString());
}
```
Execute this command

```
mvn test -Dtest="WebDriverBuilderJupiterTest#testSessionId"
```
 
##### Locating WebElements

One of the most relevant aspects of the Selenium WebDriver API is the ability to
interact with the different elements of a web page. These elements are handled by
Selenium WebDriver using the interface WebElement, an abstraction for HTML ele‐
ments.


##### WebElement Methods
![alt text](./images/photo_2023-10-11_11-47-38.jpg)
![alt text](./images/photo_2023-10-11_11-49-06.jpg)

##### Location Strategies
![alt text](./images/photo_2023-10-11_12-00-58.jpg)

##### Locating by HTML tag name
One of the most basic strategies for finding web elements is by tag name.

![alt text](./images/photo_2023-10-11_12-05-38.jpg)

```
<textarea class="form-control" id="my-textarea" rows="3"></textarea>
```

```
@Test
void testByTagName() {
	this.driver.get("https://bonigarcia.dev/selenium-webdriver-java/web-form.html");
	WebElement textArea = this.driver.findElement(By.tagName("textarea"));
	assertThat(textArea.getDomAttribute("rows")).isEqualTo("3");
}
```

```
mvn test -Dtest="WebDriverBuilderJupiterTest#testByTagName"
```
 
##### Locating by HTML attributes (name, id, class)
Another straightforward location strategy is to find web elements by an HTML
attribute, i.e., name, id, or class. Consider the following input text available in the
practice web form.

![alt text](./images/photo_2023-10-11_13-02-49.jpg)

```
<input type="text" class="form-control" name="my-text" id="my-text-id"
myprop="myvalue">
```

```
@Test
void testByHtmlAttributes() {
	this.driver.get("https://bonigarcia.dev/selenium-webdriver-java/web-form.html");
	WebElement textByName = this.driver.findElement(By.name("my-text"));
	assertThat(textByName.isEnabled()).isTrue();
		
	WebElement textById = this.driver.findElement(By.id("my-text-id"));
	assertThat(textById.getAttribute("type")).isEqualTo("text");
	assertThat(textById.getDomAttribute("type")).isEqualTo("text");
	assertThat(textById.getDomProperty("type")).isEqualTo("text");
			
	assertThat(textById.getAttribute("myprop")).isEqualTo("myvalue");
	assertThat(textById.getDomAttribute("myprop")).isEqualTo("myvalue");
	assertThat(textById.getDomProperty("myprop")).isNull();
		
	List<WebElement> byClassName = this.driver.findElements(By.className("form-control"));
	assertThat(byClassName.size()).isPositive();
	assertThat(byClassName.get(0).getAttribute("name")).isEqualTo("my-text");
}
```

```
mvn test -Dtest="WebDriverBuilderJupiterTest#testByHtmlAttributes"
```

##### Locating by link text

The last basic locator is by link text. This strategy is twofold: locate by exact and by
partial text occurrence.
 
![alt text](./images/photo_2023-10-11_16-37-21.jpg)

```
<a href="./index.html">Return to index</a>
```

```
@Test
void testByLinkText() {
	this.driver.get("https://bonigarcia.dev/selenium-webdriver-java/web-form.html");	
	WebElement linkByText = this.driver.findElement(By.linkText("Return to index"));
	assertThat(linkByText.getTagName()).isEqualTo("a");
	assertThat(linkByText.getCssValue("cursor")).isEqualTo("pointer");
	
	WebElement linkByPartialText = this.driver.findElement(By.partialLinkText("index"));
	assertThat(linkByPartialText.getLocation()).isEqualTo(linkByText.getLocation());
	assertThat(linkByPartialText.getRect()).isEqualTo(linkByText.getRect());
}
```

```
mvn test -Dtest="WebDriverBuilderJupiterTest#testByLinkText"
```

##### Locating by CSS selectors
The strategies we have seen so far are easy to apply but also have some limitations.
First, locating by tag name can be tricky since it is likely that the same tag will occur
many times on a web page. Next, finding elements by HTML attributes (name, id, or
class) is a limited approach since these attributes are not always available. In addition,
ids can be autogenerated and volatile between different sessions. Lastly, the location
by link text is limited only to links. To overcome these limitations, Selenium Web‐
Driver provides two powerful location strategies: CSS selector and XPath.

![alt text](./images/photo_2023-10-11_16-53-31.jpg)
![alt text](./images/photo_2023-10-11_16-58-39.jpg)

```
<input type="hidden" name="my-hidden">
```

```
@Test
void testByCssSelectorBasic() {
	this.driver.get("https://bonigarcia.dev/selenium-webdriver-java/web-form.html");
		
	WebElement hidden = this.driver.findElement(By.cssSelector("input[type=hidden]"));
	assertThat(hidden.isDisplayed()).isFalse();
}
```
