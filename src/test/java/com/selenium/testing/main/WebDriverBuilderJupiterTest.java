package com.selenium.testing.main;

import static org.assertj.core.api.Assertions.assertThat;
import static java.lang.invoke.MethodHandles.lookup;
import static org.slf4j.LoggerFactory.getLogger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.slf4j.Logger;
import java.util.List;
import io.github.bonigarcia.wdm.WebDriverManager;

public class WebDriverBuilderJupiterTest {
	public WebDriver driver;
    static final Logger log = getLogger(lookup().lookupClass());
	
	@BeforeAll
	static void setupClass() {
		WebDriverManager.chromedriver().setup();
	}
	
	@BeforeEach
	void setup() {
		this.driver = RemoteWebDriver.builder().oneOf(new SafariOptions())
				.addAlternative(new ChromeOptions()).build();
	}
	
	@AfterEach
	void teardown() {
		this.driver.quit();
	}
	
	@Test
	void testBasicMethods() {
		String sutUrl =  "https://bonigarcia.dev/selenium-webdriver-java/";
		this.driver.get(sutUrl);
		assertThat(this.driver.getTitle())
			.isEqualTo("Hands-On Selenium WebDriver with Java");
		assertThat(this.driver.getCurrentUrl()).isEqualTo(sutUrl);
		assertThat(this.driver.getPageSource()).containsIgnoringCase("</html>");
	}
	
	@Test
	void testSessionId() {
		this.driver.get("https://bonigarcia.dev/selenium-webdriver-java/");
		SessionId sessionId = ((RemoteWebDriver) driver).getSessionId();
		assertThat(sessionId).isNotNull();
		log.debug("The sessionId is {}", sessionId.toString());
	}
	
	@Test
	void testByTagName() {
		this.driver.get("https://bonigarcia.dev/selenium-webdriver-java/web-form.html");
		WebElement textArea = this.driver.findElement(By.tagName("textarea"));
		assertThat(textArea.getDomAttribute("rows")).isEqualTo("3");
	}
	
	@Test
	void testByHtmlAttributes() {
		this.driver.get("https://bonigarcia.dev/selenium-webdriver-java/web-form.html");
		
		WebElement textByName = this.driver.findElement(By.name("my-text"));
		assertThat(textByName.isEnabled()).isTrue();
		
		WebElement textById = this.driver.findElement(By.id("my-text-id"));
		assertThat(textById.getAttribute("type")).isEqualTo("text");
		assertThat(textById.getDomAttribute("type")).isEqualTo("text");
		assertThat(textById.getDomProperty("type")).isEqualTo("text");
		
		assertThat(textById.getAttribute("myprop")).isEqualTo("myvalue");
		assertThat(textById.getDomAttribute("myprop")).isEqualTo("myvalue");
		assertThat(textById.getDomProperty("myprop")).isNull();
		
		List<WebElement> byClassName = this.driver.findElements(By.className("form-control"));
		assertThat(byClassName.size()).isPositive();
		assertThat(byClassName.get(0).getAttribute("name")).isEqualTo("my-text");
	}
	
	@Test
	void testByLinkText() {
		this.driver.get("https://bonigarcia.dev/selenium-webdriver-java/web-form.html");
		
		WebElement linkByText = this.driver.findElement(By.linkText("Return to index"));
		assertThat(linkByText.getTagName()).isEqualTo("a");
		assertThat(linkByText.getCssValue("cursor")).isEqualTo("pointer");
		
		WebElement linkByPartialText = this.driver.findElement(By.partialLinkText("index"));
		assertThat(linkByPartialText.getLocation()).isEqualTo(linkByText.getLocation());
		assertThat(linkByPartialText.getRect()).isEqualTo(linkByText.getRect());
	}
	
	@Test
	void testByCssSelectorBasic() {
		this.driver.get("https://bonigarcia.dev/selenium-webdriver-java/web-form.html");
		
		WebElement hidden = this.driver.findElement(By.cssSelector("input[type=hidden]"));
		assertThat(hidden.isDisplayed()).isFalse();
	}
}
