package com.selenium.testing.main;


import static java.lang.invoke.MethodHandles.lookup;
import static org.assertj.core.api.Assertions.assertThat;
import static org.slf4j.LoggerFactory.getLogger;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import io.github.bonigarcia.wdm.WebDriverManager;


public class FirefoxJupiterTest {
	static final Logger log = getLogger(lookup().lookupClass());
	WebDriver driver;
	
	@BeforeAll
    static void setupClass() {
        WebDriverManager.firefoxdriver().setup();
    }
	
	 @BeforeEach
	 void setup() {
		 System.setProperty("webdriver.gecko.driver", "C:\\Users\\rsalcedp\\.cache\\selenium\\geckodriver\\win64\\0.33.0");
		 System.setProperty("webdriver.firefox.bin", "C:\\Users\\rsalcedp\\AppData\\Local\\Mozilla Firefox");
		 this.driver = new FirefoxDriver();
	 }
	 
	 @AfterEach
	 void teardown() {
		 this.driver.quit();
	 }
	 
	 @Test
	 void  test() {
		 String sutUrl = "https://bonigarcia.dev/selenium-webdriver-java/";
		 this.driver.get(sutUrl);
		 String title = this.driver.getTitle();
		 log.debug("The title of {} is {}", sutUrl, title);
		 assertThat(title).isEqualTo("Hands-On Selenium WebDriver with Java");
	 }
}
