package com.selenium.testing.main;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;

public class ChromeJupiterTest {
	WebDriver driver;
	
	@BeforeAll
	static void setupClass() {
		WebDriverManager.chromedriver().setup();
	}
	
	@BeforeEach
	void setup() {
		this.driver = new ChromeDriver();
	}
	
	@AfterEach
	void teardown() {
		this.driver.quit();
	}

    @Test
    public void test()
    {
    	this.driver.get("https://bonigarcia.dev/selenium-webdriver-java/");
    	assertThat(driver.getTitle()).contains("Selenium WebDriver");
    }
}